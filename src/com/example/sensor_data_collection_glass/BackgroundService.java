package com.example.sensor_data_collection_glass;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Environment;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;

public class BackgroundService extends Service implements SensorEventListener{
	WakeLock wakeLock =null;
	SensorManager mSensorManager;
	Sensor mOrientation;
	Sensor mAccelerometer;
	Sensor mGyroscope; 
	float azimuth_angle;
	float pitch_angle;
	float roll_angle;
	float acceleration_x;
	float acceleration_y;
	float acceleration_z;
	float angular_x;
	float angular_y;
	float angular_z;
	static final String TAG_BACKGROUND = "background";
	BufferedWriter mBufferedWriter;
	File logfile;
	static final String TAG_STACK_TRACE = "stack_trace";
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		if(event.sensor==mAccelerometer){
			acceleration_x = event.values[0];
			acceleration_y = event.values[1];
			acceleration_z = event.values[2];
			
			//Log.d(TAG_BACKGROUND, "Accelerometer changes");
			try {
				mBufferedWriter.append("acc,");
				mBufferedWriter.append(
						String.format("%06d, %6.3f, %6.3f, %6.3f", 
								event.timestamp,
								acceleration_x,
								acceleration_y,
								acceleration_z));
				mBufferedWriter.newLine();
			} catch (Exception e) {
				//e.printStackTrace();
			}
		}
		if(event.sensor==mOrientation){
			
			azimuth_angle = event.values[0];
			pitch_angle = event.values[1];
			roll_angle = event.values[2];

			//Log.d(TAG_BACKGROUND, "Orientation changes");
			try {
				mBufferedWriter.append("ori,");
				mBufferedWriter.append(
						String.format("%06d, %6.3f, %6.3f, %6.3f", 
								event.timestamp,
								azimuth_angle,
								pitch_angle,
								roll_angle));
				mBufferedWriter.newLine();
			} catch (Exception e) {
				//e.printStackTrace();
			}
		}
		if(event.sensor==mGyroscope){
			angular_x = event.values[0];
			angular_y = event.values[1];
			angular_z = event.values[2];
			
			//Log.d(TAG_BACKGROUND, "Gyroscope changes");
			try {
				mBufferedWriter.append("gyr,");
				mBufferedWriter.append(
						String.format("%06d, %6.3f, %6.3f, %6.3f", 
								event.timestamp,
								angular_x,
								angular_y,
								angular_z));
				mBufferedWriter.newLine();
				
			} catch (Exception e) {
				//e.printStackTrace();
			}
		}
		
		
	}

	@Override
	public void onCreate() {
		acquireLock();
		initSensors();
		initBufferedWriter();
		
		super.onCreate();
	}

	private void initBufferedWriter() {
		String timeStamp = (new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()));
		File subdir = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+Setting.DATA_DIRECTORY);
		if(!subdir.exists()){
			subdir.mkdir();
		}
		
		logfile = new File(subdir,"datalog_"+timeStamp);
		
		if (!logfile.exists()){
			try{
				logfile.createNewFile();
				
			}
			catch(IOException e){
				e.printStackTrace();
			}
		}
		Log.d(TAG_STACK_TRACE, "why?");
		try {
			mBufferedWriter = new BufferedWriter(new FileWriter(logfile,true));
			if(mBufferedWriter!=null){
				Log.d(TAG_STACK_TRACE, "INITIALIZE mBufferedWriter");
			} else {
				Log.d(TAG_STACK_TRACE, "Failed: INITIALIZE mBufferedWriter");
			}
		} catch (IOException e) {
			Log.d(TAG_STACK_TRACE, "EXCEPTION?");
			e.printStackTrace();
		}
		
	}

	private void initSensors() {
		mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		if(Setting.sensor_accelerometer){
			mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		}
		if(Setting.sensor_gyroscope){
			mGyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
		}
		if(Setting.sensor_orientation){
			mOrientation = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
		}
	}

	private void acquireLock() {
		PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
		wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
		        "MyWakelockTag");
		wakeLock.acquire();
		
	}

	@Override
	public void onDestroy() {
		if(wakeLock!=null){
			if(wakeLock.isHeld()){
				wakeLock.release();
			}
		}
		mSensorManager.unregisterListener(this);
		try {
			mBufferedWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		super.onDestroy();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		registerSensor();
		return super.onStartCommand(intent, flags, startId);
	}

	private void registerSensor() {
		if(mOrientation!=null){
			mSensorManager.registerListener(this, mOrientation,
					Setting.sensor_frequency);
			//Log.d(TAG_SENSOR, "Registered Orientation");
		}
		if(mAccelerometer!=null){
			mSensorManager.registerListener(this, mAccelerometer, 
					Setting.sensor_frequency);
			
		}
		if(mGyroscope!=null){
			mSensorManager.registerListener(this, mGyroscope, Setting.sensor_frequency);
		}
		
	}




}
