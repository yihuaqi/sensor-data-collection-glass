package com.example.sensor_data_collection_glass;

import java.io.IOException;
import java.io.InputStream;
import java.io.InvalidObjectException;
import java.io.OutputStream;
import java.util.UUID;




import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings.Secure;
import android.util.Log;

public class BluetoothService {

	//private static final UUID MY_UUID = UUID.fromString(Setting.UUID);
	
    private final BluetoothAdapter mAdapter;
    private final Handler mHandler;
    private AcceptThread mAcceptThread;
    
    private ConnectThread mConnectThread;
    private ConnectedThread mConnectedThread;
    
    private int mState;
    // Name for the SDP record when creating server socket
    private static final String NAME_SECURE = "BluetoothTestSecure";
    private static final String NAME_INSECURE = "BluetoothTestInsecure";

    // Unique UUID for this application
    private static final UUID MY_UUID_SECURE =
        UUID.fromString("fa87c0d0-afac-11de-8a39-0800200c9a67");
    private static final UUID MY_UUID_INSECURE =
        UUID.fromString("8ce255c0-200a-11e0-ac64-0800200c9a67");
    
    // Constants that indicate the current connection state 
    public static final int STATE_NONE = 0;       // we're doing nothing
    public static final int STATE_LISTEN = 1;     // now listening for incoming connections
    public static final int STATE_CONNECTING = 2; // now initiating an outgoing connection
    public static final int STATE_CONNECTED = 3;  // now connected to a remote device
    public static final String TAG = "BS tag";
	public BluetoothService(Context context,Handler handler) {
		mAdapter = BluetoothAdapter.getDefaultAdapter();
		mHandler = handler;
		// TODO Auto-generated constructor stub
	}
	
	private void setState(int state){
		Log.d(TAG, "setState()" + mState + "->" + state);
		mState = state;
	}

	public void start(){
		if(mConnectThread != null) {
			mConnectThread.cancle();
			mConnectThread = null;
		}
		
		if(mConnectedThread !=null) {
			mConnectedThread.cancle();
			mConnectedThread = null;
		}
		
		setState(STATE_LISTEN);
		
		if(mAcceptThread ==null) {
			mAcceptThread = new AcceptThread(true);
			mAcceptThread.start();
		}
		
		
	}
	
	public void connect(BluetoothDevice device, boolean secure) {
		if(mState == STATE_CONNECTING){
			if(mConnectedThread != null) {
				mConnectThread.cancle();
				mConnectThread = null;
			}
		}
		if(mConnectedThread != null){
			mConnectedThread.cancle();
			mConnectedThread = null;
		}
		
		mConnectThread = new ConnectThread(device,secure);
		mConnectThread.start();
		setState(STATE_CONNECTING);
		
	}
	
	public void connected(BluetoothSocket socket,BluetoothDevice device, final String socketType){
		if(mConnectThread!=null){
			mConnectThread.cancle();
			mConnectThread = null;

		}
		
		if(mConnectedThread !=null){
			mConnectedThread.cancle();
			mConnectedThread=null;
		}
		if(mAcceptThread !=null){
			mAcceptThread.cancle();
			mAcceptThread = null;
		}
		
		mConnectedThread = new ConnectedThread(socket,socketType);
		mConnectedThread.start();
		
		Message msg = mHandler.obtainMessage(BluetoothTestActivity.MESSAGE_DEVICE_NAME);
		Bundle bundle = new Bundle();
		bundle.putString(BluetoothTestActivity.DEVICE_NAME, device.getName());
		msg.setData(bundle);
		mHandler.sendMessage(msg);
		
		setState(STATE_CONNECTED);
	}
	
	
	public void stop() {
		if(mConnectThread != null){
			mConnectThread.cancle();
			mConnectThread = null;
		}
		
		if(mConnectedThread != null){
			mConnectedThread.cancle();
			mConnectedThread = null;
		}
		
		if(mAcceptThread != null){
			mAcceptThread.cancle();
			mAcceptThread = null;
		}
		setState(STATE_NONE);
	}

	public void write(byte[] out) {
		ConnectedThread r;
		synchronized (this) {
			if(mState != STATE_CONNECTED) return;
			r = mConnectedThread;
			
		}
		r.write(out);
	}


	private void connectionFailed() {
		Log.d(TAG, "Connection Failed");
		BluetoothService.this.start();
	}
	
	private void connectionLost(){
		Log.d(TAG, "Connection Lost");
		BluetoothService.this.start();
	}
	
	class AcceptThread extends Thread{
		final BluetoothServerSocket mmServerSocket;
		String mSocketType;
		
		public AcceptThread(boolean secure){
			BluetoothServerSocket tmp = null;
			mSocketType = secure ? "Secure":"Insecure";
			try{
				if(secure){
					tmp = mAdapter.listenUsingRfcommWithServiceRecord(NAME_SECURE, MY_UUID_SECURE); 
				} else {
					tmp = mAdapter.listenUsingInsecureRfcommWithServiceRecord(NAME_INSECURE,MY_UUID_INSECURE);
				}
				
			} catch (IOException e){
				Log.e(TAG, "Socket Type: " + mSocketType + "listen() failed",e);
			}
			mmServerSocket = tmp;
		}
		
		public void run(){
			setName("AcceptThread" + mSocketType);
			BluetoothSocket socket = null;
			
			while(mState != STATE_CONNECTED){
				try{
					socket = mmServerSocket.accept();
				} catch (IOException e){
					Log.e(TAG, "Socket Type: " + mSocketType + "accept() failed", e);
                    break;
				}
				
				if(  socket != null){
					synchronized (BluetoothService.this) {
                        switch (mState) {
                        case STATE_LISTEN:
                        case STATE_CONNECTING:
                            // Situation normal. Start the connected thread.
                            connected(socket, socket.getRemoteDevice(),
                                    mSocketType);
                            break;
                        case STATE_NONE:
                        case STATE_CONNECTED:
                            // Either not ready or already connected. Terminate new socket.
                            try {
                                socket.close();
                            } catch (IOException e) {
                                Log.e(TAG, "Could not close unwanted socket", e);
                            }
                            break;
						}
						
					}
				}
				
			}
		}
		public void cancle() {
			try{
				mmServerSocket.close();
				
			} catch(IOException e){
				   Log.e(TAG, "Socket Type" + mSocketType + "close() of server failed", e);
			}
			
		}
		
	}
	
	class ConnectThread extends Thread{
		private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;
        private String mSocketType;

		public ConnectThread(BluetoothDevice device, boolean secure) {
			mmDevice = device;
			BluetoothSocket tmp = null;
			mSocketType = secure ? "Secure" : "Insecure";
			
			try {
				if(secure){
					tmp = device.createRfcommSocketToServiceRecord(MY_UUID_SECURE);
				} else {
					tmp = device.createInsecureRfcommSocketToServiceRecord(MY_UUID_INSECURE);
				}
			} catch(IOException e){
				 Log.e(TAG, "Socket Type: " + mSocketType + "create() failed", e);
			}
			mmSocket = tmp;
		}


		
		public void run(){
			setName("ConnectThread" + mSocketType);
			mAdapter.cancelDiscovery();
			try{
				mmSocket.connect();
			} catch(IOException e){
				try{
					mmSocket.close();
				} catch (IOException e2){
					 Log.e(TAG, "unable to close() " + mSocketType +
	                            " socket during connection failure", e2);
				}
				connectionFailed();
				return;
			}
			
			synchronized (BluetoothService.this) {
				mConnectThread = null;
				
			}
			
			connected(mmSocket, mmDevice, mSocketType);
		}
		
		public void cancle(){
			try{
				mmSocket.close();
			} catch(IOException e){
				Log.e(TAG, "close() of connect " + mSocketType + " socket failed", e);
			}
		}
		
	}
	class ConnectedThread extends Thread{
		private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;
		public ConnectedThread(BluetoothSocket socket, String socketType) {
            Log.d(TAG, "create ConnectedThread: " + socketType);
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the BluetoothSocket input and output streams
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                Log.e(TAG, "temp sockets not created", e);
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
		}
		
		public void run(){
			byte[] buffer = new byte[1024];
			int bytes;
			
			while(true){
				try{
					bytes= mmInStream.read(buffer);
					mHandler.obtainMessage(BluetoothTestActivity.MESSAGE_RECEIVE_RESPONSE,bytes,-1,buffer).sendToTarget();
				} catch (IOException e){
                    Log.e(TAG, "disconnected", e);
                    connectionLost();
                    // Start the service over to restart listening mode
                    BluetoothService.this.start();
                    break;
				}
			}
		}

		public void write(byte[] buffer) {
			try{
				mmOutStream.write(buffer);
				
				//mHandler.obtainMessage(BluetoothTestActivity.MESSAGE_WRITE,-1,-1,buffer).sendToTarget();
				
			} catch (IOException e){
				Log.e(TAG, "Exception during write", e);
			}
			
		}

		public void cancle() {
			try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "close() of connect socket failed", e);
            }
			
		}
		
	}

}
