package com.example.sensor_data_collection_glass;



import java.lang.reflect.Method;
import java.util.Set;






import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class BluetoothTestActivity extends Activity{

	Button mGPSSyncButton;
    // Name of the connected device
    private String mConnectedDeviceName = null;
    // Array adapter for the conversation thread
    private ArrayAdapter<String> mConversationArrayAdapter;
    // String buffer for outgoing messages
    private StringBuffer mOutStringBuffer;
    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;
    // Member object for the chat services
    private BluetoothService mBluetoothTestService = null;
    private LocationManager mLocationManager;
    public static final int MESSAGE_RECEIVE_RESPONSE = 0;
    public static final int MESSAGE_DEVICE_NAME = 1;
    public static final int MESSAGE_WRITE = 2;
    public static final String TAG = "BS tag";
    public static final String DEVICE_NAME = "device_name";
    boolean isTesting = false; 
    Button mStartButton;
    private Button mNTPSyncButton;
    TextView mResultText;
	long sentTimeStamp;
	StringBuffer mStringBuffer;
    public static final String SENT_SIGNAL = "s";
    public static final String RECEIVE_SIGNAL = "r";
    long timeOffset=0;
    @Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		getWindow().addFlags(LayoutParams.FLAG_KEEP_SCREEN_ON);
		Log.d(TAG, "+++ ON CREATE +++");
		setContentView(R.layout.bluetooth_test);
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if(mBluetoothAdapter == null){
			Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_SHORT).show();
			finish();
			return;
		}
		mStringBuffer = new StringBuffer();
		mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
	}

 LocationListener mLocationListener = new LocationListener() {
		
		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onLocationChanged(Location location) {
			Log.d(TAG,"Get GPS signal");
			SyncClock(location);
			
		}
	};
	@Override
	protected void onDestroy() {
		
		super.onDestroy();
		if(mBluetoothTestService!=null) mBluetoothTestService.stop();
		
	}

	protected void SyncClock(Location location) {
		timeOffset = location.getTime() - System.currentTimeMillis();
		mStringBuffer.append("Synchronize time to "+location.getTime()+"\n");
		mResultText.setText(mStringBuffer);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onStart() {
		
		super.onStart();
		Log.e(TAG, "++ ON START ++");
		if(!mBluetoothAdapter.isEnabled()){
			Toast.makeText(this,"Please pair your devices first", Toast.LENGTH_SHORT).show();
			
		} else {
			if(mBluetoothTestService == null) setupTest();
		}
		
		Set<BluetoothDevice> devices = mBluetoothAdapter.getBondedDevices();
		BluetoothDevice device = devices.iterator().next();
		mBluetoothTestService.connect(device,true);
		
		
	}
	OnClickListener mOnClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			
			if(v.getId() == R.id.button_bluetooth_test_start){
				if(!isTesting){
			
					isTesting = true;
					byte[] send = SENT_SIGNAL.getBytes();
					mBluetoothTestService.write(send);
					sentTimeStamp = System.currentTimeMillis()+timeOffset;
					mStringBuffer.append("Sent on "+sentTimeStamp);
					Log.d(TAG, "Sent: " + SENT_SIGNAL);
					mStringBuffer.append('\n');
					mResultText.setText(mStringBuffer);
				}
			}
			if(v.getId()== R.id.button_bluetooth_test_GPS){
				mLocationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER, mLocationListener, null);
				Log.d(TAG, "RequestSingleUpdate");
			}
			if(v.getId() == R.id.button_bluetooth_test_NTP){
				
			}
			/*
			if(isTesting){
				mStartButton.setText("Start");
				isTesting = false;
			} else {
				mStartButton.setText("Stop");
				isTesting = true;
			}
			*/
			mStartButton.setText("Check");
		}
	};
	
	
	 
	
	private void setupTest() {
		mResultText = (TextView) findViewById(R.id.text_bluetooth_test_result);
		mStartButton = (Button) findViewById(R.id.button_bluetooth_test_start);
		mStartButton.setOnClickListener(mOnClickListener);
		mGPSSyncButton = (Button) findViewById(R.id.button_bluetooth_test_GPS);
		mStartButton.setOnClickListener(mOnClickListener);
		mNTPSyncButton = (Button) findViewById(R.id.button_bluetooth_test_NTP);
		mNTPSyncButton.setOnClickListener(mOnClickListener);
		mBluetoothTestService = new BluetoothService(this,mHandler);
		mGPSSyncButton.setOnClickListener(mOnClickListener);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}
	
	private Handler mHandler = new Handler(){

		@Override
		public void handleMessage(Message msg) {
			switch(msg.what){
			case MESSAGE_RECEIVE_RESPONSE:
				byte[] readBuf = (byte[]) msg.obj;
                // construct a string from the valid bytes in the buffer
                String readMessage = new String(readBuf, 0, msg.arg1);
                long receivedTimeStamp = System.currentTimeMillis()+timeOffset;
                
                if(readMessage.equals(SENT_SIGNAL)){
					byte[] send = RECEIVE_SIGNAL.getBytes();
					mBluetoothTestService.write(send);
    				mStringBuffer.append("Received on "+receivedTimeStamp);
    				mStringBuffer.append('\n');
    				mResultText.setText(mStringBuffer);
    				mStringBuffer.setLength(0);
                }
                if(readMessage.equals(RECEIVE_SIGNAL)){
    				
    				mStringBuffer.append("Received on "+receivedTimeStamp);
    				mStringBuffer.append('\n');
    				mStringBuffer.append("Subtraction: "+(receivedTimeStamp-sentTimeStamp));
    				mStringBuffer.append('\n');
    				mResultText.setText(mStringBuffer);
    				isTesting = false;
    				mStringBuffer.setLength(0);
                }
				break;
			case MESSAGE_DEVICE_NAME:
				mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
				mStringBuffer.append("Connected to "+mConnectedDeviceName+"\n");
				mResultText.setText(mStringBuffer);
				break;
				
			case MESSAGE_WRITE:
				//mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
				//mStringBuffer.append("Sent on "+System.currentTimeMillis()+"\n");
				//mResultText.setText(mStringBuffer);
				break;
			}
			super.handleMessage(msg);
		}
		
	};

}
