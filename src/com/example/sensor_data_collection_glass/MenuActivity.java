package com.example.sensor_data_collection_glass;

import com.google.android.glass.app.Card;
import com.google.android.glass.timeline.TimelineManager;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.ClipData.Item;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager.LayoutParams;

public class MenuActivity extends Activity {

	MenuItem set_Mode;

	MenuItem start;
	MenuItem stop;
	
	static final String TAG_MENU = "google_menu";
	static final String TAG_SHAREDPREFS = "share";
	static final int SET_MODE = 0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().addFlags(LayoutParams.FLAG_KEEP_SCREEN_ON);
        Card card = new Card(this);
        card.setText("Sensor Data Collection Test");
        card.setFootnote("Tap to activate the menu");
        
        setContentView(card.toView());

        
        // Load user's preference;
		loadPreference();
		Log.d(TAG_SHAREDPREFS, "LOAD PREFERENCE");	
	}

	private void findAllMenuItems(Menu menu) {
		set_Mode = menu.findItem(R.id.set_mode);

		start = menu.findItem(R.id.start);
		stop = menu.findItem(R.id.stop);
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu, menu);
		findAllMenuItems(menu);
		//set_Mode.setTitle(R.string.set_mode_with_image_not_save);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case R.id.set_mode:
			Intent setModeIntent = new Intent(this,SetModeActivity.class);
			startActivity(setModeIntent);
			
			
			break;
		case R.id.set_sensor:
			Intent setSensorIntent = new Intent(this,SetSensorActivity.class);
			startActivity(setSensorIntent);
			break;
		case R.id.set_frequency:
			Intent setFrequencyIntent = new Intent(this,SetFrequencyActivity.class);
			startActivity(setFrequencyIntent);
			break;
		case R.id.start:
			//Setting.isRunning = false;
			Intent startMeasureIntent;
			switch (Setting.mode) {
			case Setting.MODE_WITHOUT_IMAGE_VIEW:
				startMeasureIntent = new Intent(this,WithoutImageViewActivity.class);
				startActivity(startMeasureIntent);				
				break;
			case Setting.MODE_BACKGROUND:
				startService(new Intent(this,BackgroundService.class));
				Setting.isRunning = true;
				break;
			case Setting.MODE_WITH_IMAGE_VIEW:
				startMeasureIntent = new Intent(this,WithImageViewActivity.class);
				startActivity(startMeasureIntent);
				break;
			default:
				break;
			}
			break;
		case R.id.stop:
			
			stopService(new Intent(this,BackgroundService.class));
			Setting.isRunning = false;
			break;
		case R.id.bluetooth_test:
			startActivity(new Intent(this,BluetoothTestActivity.class));
			break;
			
		}
		//return super.onOptionsItemSelected(item);
		return true;
	}

	@Override
	public void onOptionsMenuClosed(Menu menu) {
		// TODO Auto-generated method stub
		super.onOptionsMenuClosed(menu);
		
		//finish();
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		menu.findItem(R.id.stop).setVisible(Setting.isRunning);
		menu.findItem(R.id.start).setVisible(!Setting.isRunning);
		
		return super.onPrepareOptionsMenu(menu);
	}


	// Save setting parameters into SharedPreferences 
	public void savePreference(){
		SharedPreferences.Editor editor = getPreferences(MODE_PRIVATE).edit();
		editor.putInt(Setting.KEY_MODE, Setting.mode);
		Log.d(TAG_SHAREDPREFS, "PUT KEY_MODE = "+Setting.mode);
		editor.putBoolean(Setting.KEY_SAVEIMAGE, Setting.saveImage);
		Log.d(TAG_SHAREDPREFS,"PUT KEY_SAVIMAGE = "+Setting.saveImage);
		editor.putBoolean(Setting.KEY_SENSOR_ACCELEROMETER, Setting.sensor_accelerometer);
		Log.d(TAG_SHAREDPREFS, "PUT KEY_SENSOR_ACCELEROMETER = "+Setting.sensor_accelerometer);
		editor.putInt(Setting.KEY_SENSOR_FREQUENCY, Setting.sensor_frequency);
		Log.d(TAG_SHAREDPREFS, "PUT KEY_SENSOR_FREQUENCY = "+Setting.sensor_frequency);
		editor.putBoolean(Setting.KEY_SENSOR_GYROSCOPE, Setting.sensor_gyroscope);
		Log.d(TAG_SHAREDPREFS,  "PUT KEY_SENSOR_GYROSCOPE = "+Setting.sensor_gyroscope);
		editor.putBoolean(Setting.KEY_SENSOR_ORIENTATION, Setting.sensor_orientation);
		Log.d(TAG_SHAREDPREFS, "PUT KEY_SENSOR_ORIENTATION = "+Setting.sensor_orientation);
		editor.commit();
	}

	@Override
	
	/*
	 * Open the option menu when the app is launched or return from other activities.
	 */
	protected void onResume() {
		super.onResume();
		openOptionsMenu();

	}

	@Override
	public void onBackPressed() {
		
		super.onBackPressed();
		finish();
	}

	/*
	 * Save preference when this app finish
	 */
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		savePreference();
		Log.d(TAG_SHAREDPREFS, "SAVE PREFERENCE");
		super.onDestroy();
	}

	// Load setting from SharedPreferences
	public void loadPreference(){
		
		SharedPreferences prefs = getPreferences(MODE_PRIVATE);
		Setting.mode = prefs.getInt(Setting.KEY_MODE, Setting.MODE_BACKGROUND);
		Log.d(TAG_SHAREDPREFS, "GET KEY_MODE = "+Setting.mode);
		Setting.saveImage = prefs.getBoolean(Setting.KEY_SAVEIMAGE, false);
		Log.d(TAG_SHAREDPREFS, "GET KEY_SAVEIMAGE = "+Setting.saveImage);
		Setting.sensor_accelerometer = prefs.getBoolean(Setting.KEY_SENSOR_ACCELEROMETER, false);
		Log.d(TAG_SHAREDPREFS, "GET KEY_SENSOR_ACCELEROMETER = "+Setting.sensor_accelerometer);
		Setting.sensor_gyroscope = prefs.getBoolean(Setting.KEY_SENSOR_GYROSCOPE, false);
		Log.d(TAG_SHAREDPREFS, "GET KEY_SENSOR_GYROSCOPE = "+Setting.sensor_gyroscope);
		Setting.sensor_orientation = prefs.getBoolean(Setting.KEY_SENSOR_ORIENTATION, false);
		Log.d(TAG_SHAREDPREFS, "GET KEY_SENSOR_ORIENTATION = "+Setting.sensor_orientation );
		Setting.sensor_frequency = prefs.getInt(Setting.KEY_SENSOR_FREQUENCY, Setting.FREQUENCY_LOW);
		Log.d(TAG_SHAREDPREFS, "GET KEY_SENSOR_FREQUENCY = "+Setting.sensor_frequency );
		


		
		
	}
	/*
	 * Open the menu when user taps.
	 */
@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		Log.d(TAG_MENU, "KEYEVENT = "+keyCode);
		if(keyCode == KeyEvent.KEYCODE_DPAD_CENTER){
			openOptionsMenu();
			return true;
		}
		if(keyCode == KeyEvent.KEYCODE_BACK){
			Log.d(TAG_MENU, "BACK");
			finish();
		}
		return false;
	}



}
