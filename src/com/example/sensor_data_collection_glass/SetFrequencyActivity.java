package com.example.sensor_data_collection_glass;

/*
 * This activity is shown when user taps the MenuItem set_frequency
 */
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.widget.RadioButton;

public class SetFrequencyActivity extends Activity {
	RadioButton radio_frequency_high;
	RadioButton radio_frequency_mid;
	RadioButton radio_frequency_low;

	
    static final String TAG_CLICK = "tag";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.set_frequency);
		findAllViews();
    }
	// Find all views by its id.
	void findAllViews(){
		radio_frequency_high = (RadioButton) findViewById(R.id.radio_frequency_high);
		radio_frequency_low = (RadioButton) findViewById(R.id.radio_frequency_low);
		radio_frequency_mid = (RadioButton) findViewById(R.id.radio_frequency_mid);
		
	}
    @Override
    public void onResume() {
        super.onResume();
        renderPreferrence();
    }
    void renderPreferrence(){

		switch (Setting.sensor_frequency) {
		case Setting.FREQUENCY_HIGH:
			radio_frequency_high.setChecked(true);
			break;
		case Setting.FREQUENCY_LOW:
			radio_frequency_low.setChecked(true);
			break;
		case Setting.FREQUENCY_MID:
			radio_frequency_mid.setChecked(true);
			break;
		default:
			break;
		}
	}
    @Override
    public void onPause() {
        super.onPause();
        
    }
    public void onRadioButtonClicked(View view) {
		switch (view.getId()) {
		case R.id.radio_frequency_high:
			Setting.sensor_frequency = Setting.FREQUENCY_HIGH;
			
			break;
		case R.id.radio_frequency_low:
			Setting.sensor_frequency = Setting.FREQUENCY_LOW;
			
			break;
		case R.id.radio_frequency_mid:
			Setting.sensor_frequency = Setting.FREQUENCY_MID;
			
			break;
		default:
			break;
		}
    	Log.d(TAG_CLICK, "Radioed");
    }


}
