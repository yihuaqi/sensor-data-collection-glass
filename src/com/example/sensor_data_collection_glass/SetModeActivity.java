package com.example.sensor_data_collection_glass;
/*
 * This activity is shown when user tap the MenuItem set_mode. 
 */

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.widget.CheckBox;
import android.widget.RadioButton;

public class SetModeActivity extends Activity {
	RadioButton radio_withImage;
	RadioButton radio_withoutImage;
	CheckBox check_savingimage;
	RadioButton radio_background;
    
    static final String TAG_CLICK = "tag";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.set_mode);
		findAllViews();
    }
	// Find all views by its id.
	void findAllViews(){
		radio_withImage = (RadioButton) findViewById(R.id.radio_withimage);
		radio_withoutImage = (RadioButton) findViewById(R.id.radio_withoutimage);
		radio_background = (RadioButton) findViewById(R.id.radio_background);
		check_savingimage = (CheckBox) findViewById(R.id.check_saveimage);
	}
	 @Override
	    public void onResume() {
		 	
	        super.onResume();
	        // Render user's preference
	        renderPreferrence();
	    }
	    void renderPreferrence(){
			switch (Setting.mode) {
			case Setting.MODE_BACKGROUND:
				radio_background.setChecked(true);
				Log.d(TAG_CLICK, "Read Preference MODE_BACKGROUND");
				break;
			case Setting.MODE_WITH_IMAGE_VIEW:
				radio_withImage.setChecked(true);
				Log.d(TAG_CLICK, "Read Preference MODE_WITH_IMAGE_VIEW");
				break;
			case Setting.MODE_WITHOUT_IMAGE_VIEW:
				radio_withoutImage.setChecked(true);
				Log.d(TAG_CLICK, "Read Preference MODE_WITHOUT_IMAGE_VIEW");
				break;

			default:
				break;
			}
			
			check_savingimage.setChecked(Setting.saveImage);
			
		}

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG_CLICK, "MODE is "+ Setting.mode);
        
    }
    public void onRadioButtonClicked(View view) {
		switch (view.getId()) {
		case R.id.radio_background:
			Setting.mode = Setting.MODE_BACKGROUND;
			Log.d(TAG_CLICK, "SELECT MODE_BACKGROUND");
			break;
		case R.id.radio_withimage:
			Setting.mode = Setting.MODE_WITH_IMAGE_VIEW;
			Log.d(TAG_CLICK, "SELECT MODE_WITH_IMAGE_VIEW");
			break;
		case R.id.radio_withoutimage:
			Setting.mode = Setting.MODE_WITHOUT_IMAGE_VIEW;
			Log.d(TAG_CLICK, "SELECT MODE_WITHOUT_IMAGE_VIEW");
			break;
		default:
			break;
		}
    	Log.d(TAG_CLICK, "MODE is "+ Setting.mode);
    }
	public void onCheckedBoxClicked(View view){
		
		switch(view.getId()){
		
		case R.id.check_saveimage:
			Setting.saveImage = check_savingimage.isChecked();
			break;
		}
		Log.d(TAG_CLICK, "clicked");
		
	}



}
