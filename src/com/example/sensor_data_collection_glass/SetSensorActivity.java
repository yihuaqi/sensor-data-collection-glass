package com.example.sensor_data_collection_glass;

// This activity is shown when user taps the MenuItem set_sensor



import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.widget.CheckBox;

public class SetSensorActivity extends Activity {
	
	CheckBox check_gyroscope;
	CheckBox check_accelerometer;
	CheckBox check_orientation;
	
    static final String TAG_CLICK = "tag";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.set_sensor);
		findAllViews();
    }
	// Find all views by its id.
	void findAllViews(){
		check_accelerometer = (CheckBox) findViewById(R.id.check_accelerometer);
		check_gyroscope = (CheckBox) findViewById(R.id.check_gyroscope);
		check_orientation = (CheckBox) findViewById(R.id.check_orientation);
	}
	 @Override
	    public void onResume() {
	        super.onResume();
	        renderPreferrence();
	    }
	    void renderPreferrence(){

			check_accelerometer.setChecked(Setting.sensor_accelerometer);
			check_gyroscope.setChecked(Setting.sensor_gyroscope);
			check_orientation.setChecked(Setting.sensor_orientation);
			
		}

    @Override
    public void onPause() {
        super.onPause();
        
    }

	public void onCheckedBoxClicked(View view){
		
		switch(view.getId()){
		case R.id.check_accelerometer:
			Setting.sensor_accelerometer = check_accelerometer.isChecked();
			break;
		case R.id.check_gyroscope:
			Setting.sensor_gyroscope = check_gyroscope.isChecked();
			break;
		case R.id.check_orientation:
			Setting.sensor_orientation = check_orientation.isChecked();
			break;
		}
		Log.d(TAG_CLICK, "clicked");
		
	}


}
