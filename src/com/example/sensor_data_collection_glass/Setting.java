package com.example.sensor_data_collection_glass;

import android.hardware.SensorManager;
import android.provider.SyncStateContract.Constants;

public class Setting {
	// The value of mode is one of MODE_WITH_IMAGE_VIEW, MODE_WITHOUT_IMAGE_VIEW, MODE_BACKGROUND
	public static int mode;
	public static final int MODE_WITH_IMAGE_VIEW = 0;
	public static final int MODE_WITHOUT_IMAGE_VIEW = 1;
	public static final int MODE_BACKGROUND = 2;
	
	// If mode is MODE_WITH_IMAGE_VIEW and saveImage is true, then the image captured by camera will be stored.
	// If mode is MODE_WITH_IMAGE_VIEW and saveImage is false, then the image captured by camera will not be stored.
	// If mode is not MODE_WITH_IMAGE_VIEW, this flag won't matter.
	public static boolean saveImage;
	
	// If sensor_gyroscope is true, then the data from gyroscope will be measured. 
	public static boolean sensor_gyroscope;
	// If sensor_accelerometer is true, then the data from accelerometer will be measured.
	public static boolean sensor_accelerometer;
	// If sensor_orientation is true, then the data from orientation will be measured
	public static boolean sensor_orientation;
	// The value of sensor_frequency is one of FREQUENCY_HIGH, FREQUENCY_MID, and FREQUENCY_LOW.
	// This value will determine the sample rate of sensors.
	public static int sensor_frequency;
	// High sample rate.
	public static final int FREQUENCY_HIGH = SensorManager.SENSOR_DELAY_GAME;
	// Medium sample rate.
	public static final int FREQUENCY_MID = SensorManager.SENSOR_DELAY_UI;
	// Low sample rate.
	public static final int FREQUENCY_LOW = SensorManager.SENSOR_DELAY_NORMAL;
	
	// These are keys of sharedPreferences.
	
	// KEY_MODE stores mode.
	public static final String KEY_MODE = "mode";
	// KEY_SAVEIMAGE stores saveImage
	public static final String KEY_SAVEIMAGE = "saveimage";
	// KEY_SENSOR_GYROSCOPE stores sensor_gyroscope
	public static final String KEY_SENSOR_GYROSCOPE = "sensor_gyroscope";
	// KEY_SENSOR_ACCELEROMETER stores sensor_accelerometer
	public static final String KEY_SENSOR_ACCELEROMETER = "sensor_accelerometer";
	// KEY_SENSOR_ORIENTATION stores sensor_orientation
	public static final String KEY_SENSOR_ORIENTATION = "sensor_orientation";
	// KEY_SENSOR_FREQUENCY stores sensor_frequency
	public static final String KEY_SENSOR_FREQUENCY = "sensor_frequency"; 
	
	// Indicating whether this app is measuring or not.
	public static boolean isRunning = false;
	// This is the directory name for saving data files.
	public static final String DATA_DIRECTORY = "/Sensor_DC/";
}
