package com.example.sensor_data_collection_glass;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.content.Context;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.widget.TextView;

public class WithImageViewActivity extends Activity implements SurfaceHolder.Callback, SensorEventListener{
	SurfaceView mSurfaceView;
	SurfaceHolder mSurfaceHolder;
	Camera mCamera;
	TextView text_with_image;
	SensorManager mSensorManager;
	Sensor mOrientation;
	Sensor mAccelerometer;
	Sensor mGyroscope;
	float azimuth_angle;
	float pitch_angle;
	float roll_angle;
	float acceleration_x;
	float acceleration_y;
	float acceleration_z;
	float angular_x;
	float angular_y;
	float angular_z;
	boolean isRecording = false;
	static final String TAG_CAMERA = "cameratag";
	BufferedWriter mBufferedWriter;
	File logfile;
	MediaRecorder mediaRecorder;
	File mediaFile;
	File subdir;
	static final String TAG_STACK_TRACE = "stack_trace";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.with_image_view);
		mSurfaceView = (SurfaceView) findViewById(R.id.surfaceview_with_image);
		mSurfaceHolder = mSurfaceView.getHolder();
		mSurfaceHolder.addCallback(this);
		text_with_image = (TextView) findViewById(R.id.text_with_image);
		getWindow().addFlags(LayoutParams.FLAG_KEEP_SCREEN_ON);
		mediaRecorder = new MediaRecorder();
		initSensors();
		
	}
	
	private void initBufferedWriter() {
		String timeStamp = (new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()));
		//String timeStamp = System.currentTimeMillis()+"";
		
		subdir = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+Setting.DATA_DIRECTORY);
		if(!subdir.exists()){
			subdir.mkdir();
		}
		
		logfile = new File(subdir,"datalog_"+timeStamp);
		
		if (!logfile.exists()){
			try{
				logfile.createNewFile();
				
			}
			catch(IOException e){
				e.printStackTrace();
			}
		}
		Log.d(TAG_STACK_TRACE, "why?");
		try {
			mBufferedWriter = new BufferedWriter(new FileWriter(logfile,true));
			if(mBufferedWriter!=null){
				Log.d(TAG_STACK_TRACE, "INITIALIZE mBufferedWriter");
			} else {
				Log.d(TAG_STACK_TRACE, "Failed: INITIALIZE mBufferedWriter");
			}
		} catch (IOException e) {
			Log.d(TAG_STACK_TRACE, "EXCEPTION?");
			e.printStackTrace();
		}
		
		if(Setting.saveImage){
			mCamera.unlock();
			mediaRecorder.setCamera(mCamera);
			
			mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
			mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
			/*
			mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
			mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
			mediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.MPEG_4_SP);
			*/
			CamcorderProfile cpHigh = CamcorderProfile.get(CamcorderProfile.QUALITY_720P);
		    mediaRecorder.setProfile(cpHigh);  
			mediaFile = new File(subdir,"videolog_"+timeStamp+".mp4");
			mediaRecorder.setOutputFile(mediaFile.toString());
			mediaRecorder.setPreviewDisplay(mSurfaceHolder.getSurface());
			try {
				mediaRecorder.prepare();
				
			} catch (IllegalStateException e) {
				
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			mediaRecorder.start();
		}
		
		
		
	}
	private void initSensors() {
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		if(Setting.sensor_accelerometer){
			mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		} 
		if(Setting.sensor_gyroscope){
			mGyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
			
		} 
		if(Setting.sensor_orientation){
			mOrientation = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
			
		} 
		
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		
		
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		if(keyCode == KeyEvent.KEYCODE_DPAD_CENTER){
			if(!isRecording){
				text_with_image.setVisibility(View.VISIBLE);


				initBufferedWriter();
				
				
				isRecording = true;
			} else {
				text_with_image.setVisibility(View.GONE);
				isRecording = false;
				if(Setting.saveImage){
					try{
						mediaRecorder.stop();
						releaseMediaRecorder();
						mCamera.lock();
					} catch(IllegalStateException e) {
						e.printStackTrace();
					}
				}
				try {
					mBufferedWriter.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			Log.d(TAG_CAMERA, "Recoding change!");
			return true;
		}
		if(keyCode == KeyEvent.KEYCODE_BACK){
			Log.d(TAG_CAMERA, "GO BACK!");
			finish();
		}
		return false;
	}
	
	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		mCamera = null;
		try{
			mCamera = Camera.open();
		} catch (Exception e){
			
		}
		Camera.Parameters params = mCamera.getParameters();
        params.setPreviewFpsRange(30000, 30000);
        params.setPreviewSize(640,360);
        mCamera.setParameters(params);
		try{
			mCamera.setPreviewDisplay(mSurfaceHolder);
		} catch(Exception e){
			
		}
		mCamera.startPreview();
		
		
		
		
		
	}
	

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		mediaRecorder.release();
		mCamera.lock();
		
		if(mCamera!=null){
			mCamera.stopPreview();
		}
		mCamera.release();
		mCamera=null;
		
	}


	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}


	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		mSensorManager.unregisterListener(this);
		try {
			mBufferedWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(Setting.saveImage){
			releaseCamera();
			releaseMediaRecorder();
			}
		
	}
	
	
	@Override
	protected void onDestroy() {
		
		super.onDestroy();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		registerSensors();
		

	}

	private void registerSensors() {
		if(mOrientation!=null){
			mSensorManager.registerListener(this, mOrientation,
					Setting.sensor_frequency);
			//Log.d(TAG_SENSOR, "Registered Orientation");
		}
		if(mAccelerometer!=null){
			mSensorManager.registerListener(this, mAccelerometer, 
					Setting.sensor_frequency);
			
		}
		if(mGyroscope!=null){
			mSensorManager.registerListener(this, mGyroscope, Setting.sensor_frequency);
		}
		
	}
	@Override
	public void onSensorChanged(SensorEvent event) {
		if(event.sensor==mAccelerometer){
			acceleration_x = event.values[0];
			acceleration_y = event.values[1];
			acceleration_z = event.values[2];
			
			//Log.d(TAG_CAMERA, "Accelerometer changes");
			try {
				mBufferedWriter.append("acc,");
				mBufferedWriter.append(
						String.format("%06d, %6.3f, %6.3f, %6.3f", 
								event.timestamp,
								acceleration_x,
								acceleration_y,
								acceleration_z));
				mBufferedWriter.newLine();
			} catch (Exception e) {
				//e.printStackTrace();
			}
		}
		if(event.sensor==mOrientation){
			
			azimuth_angle = event.values[0];
			pitch_angle = event.values[1];
			roll_angle = event.values[2];

			//Log.d(TAG_CAMERA, "Orientation changes");
			try {
				mBufferedWriter.append("ori,");
				mBufferedWriter.append(
						String.format("%06d, %6.3f, %6.3f, %6.3f", 
								event.timestamp,
								azimuth_angle,
								pitch_angle,
								roll_angle));
				mBufferedWriter.newLine();
			} catch (Exception e) {
				//e.printStackTrace();
			}
		}
		if(event.sensor==mGyroscope){
			angular_x = event.values[0];
			angular_y = event.values[1];
			angular_z = event.values[2];
			
			//Log.d(TAG_CAMERA, "Gyroscope changes");
			try {
				mBufferedWriter.append("gyr,");
				mBufferedWriter.append(
						String.format("%06d, %6.3f, %6.3f, %6.3f", 
								event.timestamp,
								angular_x,
								angular_y,
								angular_z));
				mBufferedWriter.newLine();
				
			} catch (Exception e) {
				//e.printStackTrace();
			}
		}
		
	}
	
    private void releaseMediaRecorder(){
        if (mediaRecorder != null) {
            mediaRecorder.reset();   // clear recorder configuration
            mediaRecorder.release(); // release the recorder object
            mediaRecorder = null;
            mCamera.lock();           // lock camera for later use
        }
    }

    private void releaseCamera(){
        if (mCamera != null){
            mCamera.release();        // release the camera for other applications
            mCamera = null;
        }
    }
}
