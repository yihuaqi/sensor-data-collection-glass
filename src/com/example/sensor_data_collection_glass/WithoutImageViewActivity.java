package com.example.sensor_data_collection_glass;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.widget.TextView;

public class WithoutImageViewActivity extends Activity implements SensorEventListener{
	TextView text_gyroscope_data;
	TextView text_accelerometer_data;
	TextView text_orientation_data;
	SensorManager mSensorManager;
	Sensor mOrientation;
	Sensor mAccelerometer;
	Sensor mGyroscope;
	float azimuth_angle;
	float pitch_angle;
	float roll_angle;
	float acceleration_x;
	float acceleration_y;
	float acceleration_z;
	float angular_x;
	float angular_y;
	float angular_z;
	BufferedWriter mBufferedWriter;
	File logfile;
	static final String TAG_SENSOR = "sensor";
	static final String TAG_STACK_TRACE = "stack_trace";
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getWindow().addFlags(LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.without_image_view);
		findAllViews();
		
		
		initSensors();
		initBufferedWriter();
		
	}
	
	private void initBufferedWriter() {
		String timeStamp = (new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()));
		File subdir = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+Setting.DATA_DIRECTORY);
		if(!subdir.exists()){
			subdir.mkdir();
		}
		
		logfile = new File(subdir,"datalog_"+timeStamp);
		
		if (!logfile.exists()){
			try{
				logfile.createNewFile();
				
			}
			catch(IOException e){
				e.printStackTrace();
			}
		}
		Log.d(TAG_STACK_TRACE, "why?");
		try {
			mBufferedWriter = new BufferedWriter(new FileWriter(logfile,true));
			if(mBufferedWriter!=null){
				Log.d(TAG_STACK_TRACE, "INITIALIZE mBufferedWriter");
			} else {
				Log.d(TAG_STACK_TRACE, "Failed: INITIALIZE mBufferedWriter");
			}
		} catch (IOException e) {
			Log.d(TAG_STACK_TRACE, "EXCEPTION?");
			e.printStackTrace();
		}
		
	}

	private void initSensors() {
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		if(Setting.sensor_accelerometer){
			mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		} else {
			text_accelerometer_data.setVisibility(View.GONE);
		}
		if(Setting.sensor_gyroscope){
			mGyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
			
		} else {
			text_gyroscope_data.setVisibility(View.GONE);
		}
		if(Setting.sensor_orientation){
			mOrientation = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
			
		} else {
			text_orientation_data.setVisibility(View.GONE);
		}
		
	}

	private void findAllViews() {
		text_accelerometer_data = (TextView) findViewById(R.id.text_accelerometer_data);
		text_gyroscope_data = (TextView) findViewById(R.id.text_gyroscope_data);
		text_orientation_data = (TextView) findViewById(R.id.text_orientation_data);
		
	}

	public void onSensorChanged(SensorEvent event) {
		//Log.d(TAG_SENSOR, "Sensor Change");
		if(event.sensor==mAccelerometer){
			acceleration_x = event.values[0];
			acceleration_y = event.values[1];
			acceleration_z = event.values[2];
			
			text_accelerometer_data.setText("acceleration_x: "+acceleration_x+"\n"
										+"acceleration_y: "+acceleration_y+"\n"
										+"acceleration_z: "+acceleration_z);
			
			try {
				mBufferedWriter.append("acc,");
				mBufferedWriter.append(
						String.format("%06d, %6.3f, %6.3f, %6.3f", 
								event.timestamp,
								acceleration_x,
								acceleration_y,
								acceleration_z));
				mBufferedWriter.newLine();
			} catch (Exception e) {
				//e.printStackTrace();
			}
		}
		if(event.sensor==mOrientation){
			
			azimuth_angle = event.values[0];
			pitch_angle = event.values[1];
			roll_angle = event.values[2];
			
			text_orientation_data.setText("azimuth: "+azimuth_angle+"\n"
										+"pitch: "+pitch_angle+"\n"
										+"roll: "+roll_angle);
			try {
				mBufferedWriter.append("ori,");
				mBufferedWriter.append(
						String.format("%06d, %6.3f, %6.3f, %6.3f", 
								event.timestamp,
								azimuth_angle,
								pitch_angle,
								roll_angle));
				mBufferedWriter.newLine();
			} catch (Exception e) {
				//e.printStackTrace();
			}
		}
		if(event.sensor==mGyroscope){
			angular_x = event.values[0];
			angular_y = event.values[1];
			angular_z = event.values[2];
			
			text_gyroscope_data.setText("angular_x: "+angular_x+"\n"
					+"angular_y: "+angular_y+"\n"
					+"angular_z: "+angular_z);
			try {
				mBufferedWriter.append("gyr,");
				mBufferedWriter.append(
						String.format("%06d, %6.3f, %6.3f, %6.3f", 
								event.timestamp,
								angular_x,
								angular_y,
								angular_z));
				mBufferedWriter.newLine();
				
			} catch (Exception e) {
				//e.printStackTrace();
			}
		}
	} 

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		mSensorManager.unregisterListener(this);
		

		try {
			if(mBufferedWriter!=null){
				mBufferedWriter.close();
			} else {
				Log.d(TAG_STACK_TRACE, "mBufferedWriter is NULL");
			}
		} catch (IOException e) {
			//e.printStackTrace();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		registerSensors();
		
	}

	private void registerSensors() {
		if(mOrientation!=null){
			mSensorManager.registerListener(this, mOrientation,
					Setting.sensor_frequency);
			//Log.d(TAG_SENSOR, "Registered Orientation");
		}
		if(mAccelerometer!=null){
			mSensorManager.registerListener(this, mAccelerometer, 
					Setting.sensor_frequency);
			
		}
		if(mGyroscope!=null){
			mSensorManager.registerListener(this, mGyroscope, Setting.sensor_frequency);
		}
		
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}
}
